﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFCEPF.AI107.Boutique.Entities;
using MySql.Data.MySqlClient;

namespace AFCEPF.AI107.Boutique.DataAcess
{
    public class ArticleDAO : DAO
    {
        public List<ArticleEntity> GetAll()
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 Préparer la commande
            IDbCommand cmd = GetCommand(@"SELECT * 
                                            FROM article 
                                            WHERE id_rayon IS NOT NULL");

            try
            {
                // 3 Ouvrir La connection 
                cmd.Connection.Open();


                // 4 Éxécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    result.Add(DataReaderToEntity(dr));
                }

                // 5 - Traiter le résultat

            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 Fermer la connection  
                cmd.Connection.Close();
            }

            return result;
        }

        public List<ArticleEntity> GetByIdRayon(int idRayon)
        {
            List<ArticleEntity> result = new List<ArticleEntity>();

            // 2 Préparer la commande
            IDbCommand cmd = GetCommand(@"SELECT * FROM article  " +
                                            " WHERE id_rayon = @(id_rayon) ");
            cmd.Parameters.Add(new MySqlParameter("@id_rayon", idRayon));


            try
            {
                // 3 Ouvrir La connection 
                cmd.Connection.Open();

                // 4 Éxécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {

                    result.Add(DataReaderToEntity(dr));
                }

                // 5 - Traiter le résultat


            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 Fermer la connection 
                cmd.Connection.Close();

            }


            return result;
        }



        private ArticleEntity DataReaderToEntity(IDataReader dr)
        {
            ArticleEntity article = new ArticleEntity();
            article.Id = dr.GetInt32(dr.GetOrdinal("id"));
            article.Nom = dr.GetString(dr.GetOrdinal("nom"));
            article.Prix = dr.GetFloat(dr.GetOrdinal("prix_unitaire"));
            article.Photo = dr.GetString(dr.GetOrdinal("photo"));
            article.Description = dr.GetString(dr.GetOrdinal("description"));
            article.Stock = dr.GetInt32(dr.GetOrdinal("stock"));
            article.IdRayon = dr.GetInt32(dr.GetOrdinal("id_rayon"));

            return article;
        }
    }
}
