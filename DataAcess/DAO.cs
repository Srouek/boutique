﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.DataAcess
{
    public abstract class DAO
    {
        const string connectionString = "Server=localhost;Database=boutique;Uid=root;Pwd=root;";

        public IDbCommand GetCommand(string sql)
        {
            IDbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = connectionString;

            IDbCommand cmd = new MySqlCommand();
            cmd.CommandText = sql;
            cmd.Connection = cnx;


            return cmd;
        }
    }
}
