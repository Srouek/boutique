﻿using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace AFCEPF.AI107.Boutique.DataAcess
{
    public class RayonDAO : DAO
    {
        public List<RayonEntity> GetAll()
        {
            List<RayonEntity> result = new List<RayonEntity>();

            

            // 2 Préparer la commande
            IDbCommand cmd = GetCommand("SELECT * FROM rayon");
            

            try
            {
                // 3 Ouvrir La connection 
                cmd.Connection.Open();


                // 4 Éxécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    RayonEntity rayon = new RayonEntity();
                    rayon.Id = dr.GetInt32(dr.GetOrdinal("id"));
                    rayon.Libelle = dr.GetString(dr.GetOrdinal("libelle"));
                    result.Add(rayon);

                }
            
            // 5 - Traiter le résultat
           
            }catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 Fermer la connection  
                cmd.Connection.Close();
            }
            return result;



        }

        public void InsertRayon(RayonEntity nouveaRayon)
        {

            // 2 Préparer la commande
            IDbCommand cmd = GetCommand("INSERT into Rayon (libelle) " +
                                            "VALUES (@libelle)");
        
            cmd.Parameters.Add(new MySqlParameter("@libelle", nouveaRayon.Libelle));

            try
            {
                // 3 Ouvrir La connection 
                cmd.Connection.Open();

                // 4 Éxécuter la commande
                cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat
            
            
            }catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 Fermer la connection 
                cmd.Connection.Close();

            }
      
        }
    }
}
