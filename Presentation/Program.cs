﻿using AFCEPF.AI107.Boutique.Business;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Presentation
{
    class Program
    {
        static void Main(string[] args)
        {
            AjouterUnRayon();

            AfficherListeRayon();
           
        }

        static void AjouterUnRayon()
        {
            BuCatalog bu = new BuCatalog();

            Console.WriteLine("Nom du Rayon: ");
            string nomRayon = Console.ReadLine();

            RayonEntity nouveaRayon = new RayonEntity();
            nouveaRayon.Libelle = nomRayon;

            try
            {
                bu.AjouterRayon(nouveaRayon);

            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        static void AfficherListeRayon() 
        {
            BuCatalog bu = new BuCatalog();

            foreach (RayonEntity r in bu.GetListeRayon())
            {
                Console.WriteLine("- " + r.Id + " " + r.Libelle);
            }

            Console.ReadLine();

        }
    }
}
