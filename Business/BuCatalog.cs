﻿using AFCEPF.AI107.Boutique.DataAcess;
using AFCEPF.AI107.Boutique.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Business
{
    public class BuCatalog
    {
       public List<RayonEntity> GetListeRayon()
        {
            RayonDAO dao = new RayonDAO();
            
            return dao.GetAll();
        }
        public void AjouterRayon(RayonEntity nouveaRayon)
        {
            // vérifier que le libellé est unique:
            bool trouve = false;
            //      récupérer la liste des Rayon existant
            RayonDAO dao = new RayonDAO();

            foreach(RayonEntity r in dao.GetAll())
            {
                if(r.Libelle == nouveaRayon.Libelle)
                {
                    trouve = true;
                }
            }
            if (!trouve)
            {
                // TODO: Inserer un Rayon
                dao.InsertRayon(nouveaRayon);
            }
            else
            {
                throw new Exception("ERREUR : Rayon déjà existant");
            }
        }

        // Méthode pour article

        public List<ArticleEntity> GetListeArticles()
        {
            // On peut appeler directement le type apparant puis ses méthodes
            return new ArticleDAO().GetAll();
        }

        public List<ArticleEntity> GetListeArticles(int idRayon)
        {
            ArticleDAO dao = new ArticleDAO();
            if(idRayon == -1)
            {
                return dao.GetAll();
            }
            else
            {
                return dao.GetByIdRayon(idRayon);
            }
            
        }

    }
}
