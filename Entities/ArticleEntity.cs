﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Entities
{
    public class ArticleEntity
    {
        public int Id { get; set; }
        private string nom { get; set; }
        public string Nom
        {
            get
            {
                string resultat = nom.Trim();
                return resultat.Substring(0, 1).ToUpper() + resultat.Substring(1).ToLower();
            }
            set { this.nom = value; }
        }
        public float Prix { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public int Stock { get; set; }
        public int IdRayon { get; set; }

    }
}
