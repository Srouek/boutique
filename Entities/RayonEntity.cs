﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFCEPF.AI107.Boutique.Entities
{
    public class RayonEntity
    {
        public int Id { get; set; }

        private string libelle;
        public string Libelle
        {
            get
            {
                string resultat = libelle.Trim();
                return resultat.Substring(0, 1).ToUpper() + resultat.Substring(1).ToLower();
            }

            set { this.libelle = value; }
        }

        public RayonEntity()
        {

        }

        public RayonEntity(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
            
        }
    }
}
